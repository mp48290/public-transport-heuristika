package hr.fer.heuristika.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hr.fer.heuristika.exceptions.NonFeasibleException;


public class Track {
	
	private int id;
	
	private double maxLength;
	
	private double currentLength;
	
	private int activeVehicleType; 
	
	private List<Track> blockingTracks;
	
	private List<Track> blockedTracks;
	
	private List<Vehicle> vehicles;
	
	private List<Vehicle> allowedVehicles;
	
	private boolean isBlocking;
	
	private int startTime;
	
	private int lastTime;
	
	private Map<Integer, Integer> counterOfTypes; 
	
	public Track() {
		this.maxLength = 0;
		this.currentLength = 0;
		this.blockingTracks = new ArrayList<>();
		this.blockedTracks = new ArrayList<>();
		this.vehicles = new ArrayList<>();
		this.allowedVehicles = new ArrayList<>();
		this.activeVehicleType = -1;
		this.isBlocking = false;
		this.startTime = -1;
		this.lastTime = -1;
		this.counterOfTypes = new HashMap<>();
	}
	public Track(int id) {
		this.id = id;
		this.maxLength = 0;
		this.currentLength = 0;
		this.blockingTracks = new ArrayList<>();
		this.blockedTracks = new ArrayList<>();
		this.vehicles = new ArrayList<>();
		this.allowedVehicles = new ArrayList<>();
		this.activeVehicleType = -1;
		this.isBlocking = false;
		this.startTime = -1;
		this.lastTime = -1;
		this.counterOfTypes = new HashMap<>();
	}
	
		
	public Track(int id, double maxLength, double currentLength, int activeVehicleType, List<Track> blockingTracks,
			List<Track> blockedTracks, List<Vehicle> allowedVehicles, boolean isBlocking, int startTime, int lastTime,
			Map<Integer, Integer> counterOfTypes) {
		super();
		this.id = id;
		this.maxLength = maxLength;
		this.currentLength = currentLength;
		this.activeVehicleType = activeVehicleType;
		this.blockingTracks = blockingTracks;
		this.blockedTracks = blockedTracks;
		this.allowedVehicles = allowedVehicles;
		this.isBlocking = isBlocking;
		this.startTime = startTime;
		this.lastTime = lastTime;
		this.counterOfTypes = counterOfTypes;
	}
	public void resetTrack() {
		this.vehicles = new ArrayList<>();
		this.activeVehicleType = -1;
		this.currentLength = 0;
		this.startTime = -1;
		this.lastTime = -1;
		this.counterOfTypes = new HashMap<>();
	}
	public int getStartTime() {
		return startTime;
	}
	private void setTime(int time) {
		if (this.startTime == -1 && this.lastTime == -1) {
			this.startTime = time;
			this.lastTime = time;
		} else {
			if (this.startTime > time) {
				this.startTime = time;
			} else if (this.lastTime < time) {
				this.lastTime = time;
			}
		}
		
	}
	public int getLastTime() {
		return lastTime;
	}
	
	public void setActiveVehicleType(int activeVehicleType) {
		this.activeVehicleType = activeVehicleType;
	}
	public void setBlocking(boolean isBlocking) {
		this.isBlocking = isBlocking;
	}
	public int getActiveVehicleType() {
		return activeVehicleType;
	}
	
	public boolean getIsBlocking() {
		return isBlocking;
	}
	
	public void setIsBlocking(boolean isBlocked) {
		this.isBlocking = isBlocked;
	}
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public Track setIdTrack(int id) {
		this.id = id;
		return this;
	}
	
	public double getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(double length) {
		this.maxLength = length;
	}
	public Track setMaxLengthTrack(double length) {
		this.maxLength = length;
		return this;
	}
	
	public double getCurrentLength() {
		return currentLength;
	}

	public void setCurrentLength(double length) {
		this.currentLength = length;
	}
	public Track setCurrentLengthTrack(double length) {
		this.currentLength = length;
		return this;
	}
	
	public List<Track> getBlockingTracks() {
		return blockingTracks;
	}
	
	public boolean isLengthOK(){
		return currentLength <= maxLength;
	}

	public void setBlockingTracks(List<Track> blockingTracks) {
		this.blockingTracks = blockingTracks;
	}
	public Track setBlockingTracksTrack(List<Track> blockingTracks) {
		this.blockingTracks = blockingTracks;
		return this;
	}
	
	public Track getBlockingTrack(int index) throws Exception {
		if(blockingTracks.size() > index) {
			return blockingTracks.get(index);
		}
		throw new Exception("Index greater than list size");
	}
	
	public void addBlockingTrack(Track track) {
		blockingTracks.add(track);
	}
	
	public Track getBlockedTrack(int index) throws Exception {
		if(blockedTracks.size() > index) {
			return blockedTracks.get(index);
		}
		throw new Exception("Index greater than list size");
	}
	
	public void addBlockedTrack(Track track) {
		blockedTracks.add(track);
	}
	
	public Track addBlockingTrackTrack(Track track) {
		blockingTracks.add(track);
		return this;
	}
	
	public List<Vehicle> getVehicles() {
		return vehicles;
	}

	public void setVehicles(List<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}
	
	public Track setVehiclesTrack(List<Vehicle> vehicles) {
		this.vehicles = vehicles;
		return this;
	}
	
	public Vehicle getVehicle(int index){
		return vehicles.get(index);
	}
	
	public boolean isTimeOK(){
		for (int i = 0; i < blockedTracks.size(); i++) {
			Track t1 = blockedTracks.get(i);
			if (this.startTime < t1.lastTime) {
				return false;
			}
		}
		return true;
	}
	
	public int getTimeDifference() {
		int result = 0;
		for (int i = 0; i < blockingTracks.size(); i++) {
			Track t1 = blockingTracks.get(i);

			if (this.startTime <= t1.lastTime) {
				for (Vehicle v : vehicles) {
					if (v.getTime() <= t1.lastTime) result += t1.lastTime - v.getTime();
				}			

			}
		
		}
		return result;
	}
	
	
	public void addVehicle(Vehicle v) {
		double addingSpace = 0.5;
		if (this.vehicles.isEmpty()) addingSpace = 0; //if there is not any car in row no 0.5m is added to space
		
		this.vehicles.add(v);
		this.currentLength += v.getLength() + addingSpace;
		setTime(v.getTime());
		
		this.counterOfTypes.merge(v.getType(), 1, (o,n) -> { return o++;});
	}
	
		public void addVehicleGRASP(Vehicle v) throws NonFeasibleException {
				if(activeVehicleType == -1) {
					if(this.currentLength + v.getLength() <= this.maxLength) {
						this.vehicles.add(v);
						this.activeVehicleType = v.getType();
						this.currentLength += v.getLength();
						if(this.currentLength + 0.5 <= this.maxLength) {
							this.currentLength+=0.5;
						}
					}else {
						throw new NonFeasibleException("Trying to add vehicle on too small track!");
					}
				}else {
					if(v.getType() != activeVehicleType) {
						throw new NonFeasibleException("Trying to add vehicle type to track with different vehicle type!");
					}else {
						if(this.currentLength + v.getLength() <= this.maxLength) {
							this.vehicles.add(v);
							this.currentLength += v.getLength();
					if(this.currentLength + 0.5 <= this.maxLength) {
								this.currentLength+=0.5;
							}
						}else {
							throw new NonFeasibleException("Trying to add vehicle on too small track!");
						}
						
		 			}
		 		
		 		}
		}
	
	public int getNumberOfDifferentTypes() {
		int result = 0;
		int firstType = -1;
		vehicles.sort(null);
		
		for (Vehicle v : vehicles) {
			if (firstType == -1) firstType = v.getType();
			if (firstType != v.getType()) result++;
		}
		
		return result;
	}
	
	
	//da li je vozilo dozvoljeno biti u ovoj traci
	public boolean isVehicleAllowed(Vehicle vehicle) {
		return allowedVehicles.contains(vehicle);
	}
	
	
	
	public Track addVehicleTrack(Vehicle v) {
		this.vehicles.add(v);
		return this;
	}
	public List<Vehicle> getAllowedVehicles() {
		return allowedVehicles;
	}

	public void setAllowedVehicles(List<Vehicle> allowedVehicles) {
		this.allowedVehicles = allowedVehicles;
	}
	
	public Track setAllowedVehiclesTrack(List<Vehicle> allowedVehicles) {
		this.allowedVehicles = allowedVehicles;
		return this;
	}
	
	public Vehicle getAllowedVehicle(int index) throws Exception{
		if(allowedVehicles.size() > index) {
			return allowedVehicles.get(index);
		}
		throw new Exception("Index greater than list size");
	}
	
	public void addAllowedVehicle(Vehicle v) {
		this.allowedVehicles.add(v);
	}
	
	public Track addAllowedVehicleTrack(Vehicle v) {
		this.allowedVehicles.add(v);
		return this;
	}
	@Override
	public String toString() {
		return "Track [id=" + id +", vehicles="
				+ vehicles +"]";
	}

	
	
}

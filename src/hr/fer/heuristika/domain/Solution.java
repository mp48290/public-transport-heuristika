package hr.fer.heuristika.domain;

import java.util.List;
import java.util.Random;

public class Solution implements Comparable<Solution>{
	//matrica rjesenja gdje su u redku vozila u stupcima trake
	public int[] matrixSolution;

	private int vehiclesNumber;
	
	private int tracksNumber;
	
	private List<Vehicle> vehicles;
	
	private List<Track> tracks;
	
	private double fitness;
	
	public double test;
	
	
	public Solution(List<Vehicle> vehicles2, List<Track> tracks2) {
		this.vehicles = vehicles2;
		this.tracks = tracks2;
		this.vehiclesNumber = vehicles2.size();
		this.tracksNumber = tracks2.size();
		
		matrixSolution = new int[this.vehiclesNumber];
		this.fitness = Double.MAX_VALUE;
	}
	
	public void generateRandom() {
		Random rand = new Random(System.currentTimeMillis());
		for (int i =0; i < vehiclesNumber; i++) {
			matrixSolution[i] = vehicles.get(i).getRandomAlowedTrack(rand);
		}
	}
	
	public void setMatrixSolution(int[] matrixSolution) {
		this.matrixSolution = matrixSolution;
	}
	
	public int getVehiclesNumber() {
		return vehiclesNumber;
	}
	
	public int getTracksNumber() {
		return tracksNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(fitness);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Solution other = (Solution) obj;
		if (Double.doubleToLongBits(fitness) != Double.doubleToLongBits(other.fitness))
			return false;
		return true;
	}

	@Override
	public int compareTo(Solution o) {
		return Double.compare(this.fitness, o.fitness);
	}
	
	public double getFitness() {
		return fitness;
	}
	
	public List<Vehicle> getVehicles() {
		return vehicles;
	}
	
	public List<Track> getTracks() {
		return tracks;
	}
	
	public boolean checkMatrix(Solution s) {
		for (int i =0; i < vehiclesNumber; i++) {
			if (matrixSolution[i] != s.matrixSolution[i]) {
				return false;
			}
		}
		return true;
	}
	
	public void calculateFitness() {
		fitness = 0;
		tracks.forEach((a) -> {a.resetTrack();});
		for (int i = 0; i < vehiclesNumber; i++) {
			tracks.get(matrixSolution[i]).addVehicle(vehicles.get(i));
		}
		//if there are different vehicles types in some track
		double r1 = 0;
		
		//ako neka traka krece prije nego sto bi trebala, tj. trebala bi cekati jos neku traku da zavrsi
		double r2 = 0;
		
		//ako se vozilo nalazi u traci u kojoj ne bi smio
		double r3 = 0;
		
		//ukoliko je suma trenutne duzine duza auta duza nego sto moze primiti
		double r4 = 0;
		
		
		for (int i = 0; i < tracksNumber; i++) {
			Track t = tracks.get(i);
			
			r1 += t.getNumberOfDifferentTypes();
			
			r2 += t.getTimeDifference();
			
			if (t.isLengthOK() == false) r4 += (t.getCurrentLength() - t.getMaxLength());
		}
		fitness = r1 + r2 + r3 + r4;
		test = r1;
		
	}
	
	
	@Override
	public String toString() {
		String result = "";
		for (int i = 0; i < vehiclesNumber; i++) {
			result += matrixSolution[i]+1 + " ";
		}
		return result;
	}
	
	
	
	
	
	
}

package hr.fer.heuristika.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Vehicle implements Comparable<Vehicle> {
	
	private int id;
	
	private int type;
	
	private int length;
	
	private int time;
	
	private int scheduleType;
	
	private List<Track> allowedTrack;
	
	public Vehicle(int id, int type, int length, int time, int scheduleType ) {
		this.id = id;
		this.type = type;
		this.length = length;
		this.time = time;
		this.scheduleType = scheduleType;
		this.allowedTrack = new ArrayList<>();
	}
	
	public Vehicle() {
		this.id = -1;
		this.type = -1;
		this.length = -1;
		this.time = -1;
		this.scheduleType = -1;
		this.allowedTrack = new ArrayList<>();
	}
	
	public int getAllowedTrackSize() {
		return allowedTrack.size();
	}
	
	public void addAlowedTrack(Track t) {
		allowedTrack.add(t);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Vehicle setIdVehicle(int id) {
		this.id = id;
		return this;
	}
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	
	public Vehicle setTypeVehicle(int type) {
		this.type = type;
		return this;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}
	
	public Vehicle setLengthVehicle(int length) {
		this.length = length;
		return this;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}
	
	public Vehicle setTimeVehicle(int time) {
		this.time = time;
		return this;
	}

	public int getScheduleType() {
		return scheduleType;
	}

	public void setScheduleType(int scheduleType) {
		this.scheduleType = scheduleType;
	}
	
	public Vehicle setScheduleTypeVehicle(int scheduleType) {
		this.scheduleType = scheduleType;
		return this;
	}

	@Override
	public String toString() {
		return "Vehicle [id=" + id + ", time=" + time + " ,type=" + type + "]";
	}

	@Override
	public int compareTo(Vehicle v) {
		
		return Integer.compare(this.time, v.time);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vehicle other = (Vehicle) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public int getRandomAlowedTrack(Random rand) {
		return allowedTrack.get(rand.nextInt(allowedTrack.size())).getId()-1;
	}
	
	
	
	
	
	
}

package hr.fer.heuristika.domain;

public class ParentsTuple {
	public Solution s1;
	
	public Solution s2;

	public ParentsTuple(Solution s1, Solution s2) {
		super();
		this.s1 = s1;
		this.s2 = s2;
	}

	@Override
	public String toString() {
		return "Parent [s1=" + s1 + ", s2=" + s2 + "]";
	}
	
	
}

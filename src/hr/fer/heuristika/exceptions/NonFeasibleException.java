package hr.fer.heuristika.exceptions;

//Exception that is thrown if we are trying to create non feasible solution
public class NonFeasibleException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NonFeasibleException() {
		
	}
	
	public NonFeasibleException(String msg) {
		super(msg);
	}
	
	public NonFeasibleException(Throwable cause) {
		super(cause);
	}
	
	public NonFeasibleException(String msg, Throwable cause) {
		super(msg,cause);
	}
}

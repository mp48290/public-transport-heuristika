package hr.fer.heuristika;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.InputMismatchException;

import hr.fer.heuristika.algorithm.GRASP;
import hr.fer.heuristika.algorithm.GeneticAlgorithm;
import hr.fer.heuristika.domain.Track;
import hr.fer.heuristika.domain.Vehicle;
import hr.fer.heuristika.utils.InputReaderCustom;
import hr.fer.heuristika.utils.OutputWriterCustom;

public class Main {

	public static void main(String[] args) throws FileNotFoundException {
		InputReaderCustom inputReader = new InputReaderCustom(new FileInputStream(Paths.get("").toAbsolutePath() + "\\Repository\\instanca3.txt"));

		
		ArrayList<Vehicle> vehicles = new ArrayList<>();
		ArrayList<Track> tracks = new ArrayList<>();
		
		int vehicle_number = inputReader.readInt();
		int track_number = inputReader.readInt();
		
		//Adding new Vehicles with id and length into list
		for(int i = 0; i < vehicle_number; ++i) {
			vehicles.add(new Vehicle().setIdVehicle(i+1).setLengthVehicle(inputReader.readInt()));
		}
		//Adding vehicle type into vehicle
		for(int i = 0; i < vehicle_number; ++i) {
			vehicles.get(i).setType(inputReader.readInt());
		}
		
		for (int i = 0; i < track_number; i++){ 
			tracks.add(new Track(i+1));
		}
		
		//Adding allowed vehicles into track, while creating tracks if they are not created
		for(int i = 0; i < vehicle_number; ++i) {
			for(int j = 0; j < track_number; ++j) {
				int allowed = inputReader.readInt();
				if(allowed == 1) {
					tracks.get(j).addAllowedVehicleTrack(vehicles.get(i));
					vehicles.get(i).addAlowedTrack(tracks.get(j));
				}
			}
		}
		//Adding length of each track
		for(int i = 0; i < track_number; ++i) {
			tracks.get(i).setMaxLength(inputReader.readInt());
		}
		//Adding time for each vehicle
		for(int i = 0; i < vehicle_number;++i) {
			vehicles.get(i).setTime(inputReader.readInt());
		}
		//Adding schedule type
		for(int i = 0; i < vehicle_number; ++i) {
			vehicles.get(i).setScheduleType(inputReader.readInt());
		}
		
		while(true) {
			try {
				String line = inputReader.readLine();
				boolean first = true;
				int blocking = -1;
				for (String num : line.trim().split(" ")) {
					if(first) {
						blocking = Integer.parseInt(num);
						tracks.get(blocking-1).setIsBlocking(true);
						first = false;
					}else {
						tracks.get(Integer.parseInt(num)-1).addBlockingTrack(tracks.get(blocking-1));
						tracks.get(blocking-1).addBlockedTrack(tracks.get(Integer.parseInt(num)-1));
					}
					
				}
			} catch (InputMismatchException e) {
				break;
			}
			
		}
		//500 000 minuta izvodjenja
		GRASP grasp = new GRASP(vehicles, tracks, 10
				*500000, System.currentTimeMillis());
		ArrayList<ArrayList<Integer>> solution =  grasp.run();
		OutputWriterCustom out = new OutputWriterCustom(new FileOutputStream(Paths.get("").toAbsolutePath() + "\\Repository\\GRASPresultn_3.txt"));
		for (ArrayList<Integer> t : solution) {
			String line = "";
			for(Integer v : t) {
				line += v + " ";
			}
			out.printLine(line);
		}
		out.close();
		
		//GeneticAlgorithm alg = new GeneticAlgorithm(vehicles, tracks, 10, 0.01, 10000, 9);
		//alg.run();
		
		
//		OutputWriterCustom out = new OutputWriterCustom(new FileOutputStream(Paths.get("").toAbsolutePath() + "\\Repository\\GRASPresult2_1.txt"));
//		for (Track t : solution) {
//			t.getVehicles().sort(null);
//			String line = "";
//			for(Vehicle v : t.getVehicles()) {
//				line += v.getId() + " ";
//			}
//			out.printLine(line);
//		}
//		out.close();
		

		System.out.println("gotovo!!");

	}

}

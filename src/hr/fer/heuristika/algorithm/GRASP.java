package hr.fer.heuristika.algorithm;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import javax.print.attribute.Size2DSyntax;

import hr.fer.heuristika.domain.*;
import hr.fer.heuristika.exceptions.NonFeasibleException;
public class GRASP {
	
	private ArrayList<Vehicle> vehicles;
	private ArrayList<Track> tracks;
	
	private long startTime;
	private int timeLimit;
	private Random rand;
	private long seed;
	
	private double F;
	private double G;
	
	private double bestF;
	private double bestG;
	
	public GRASP(ArrayList<Vehicle> vehicles, ArrayList<Track> tracks, int timeLimit, long seed) {
		this.vehicles = vehicles;
		this.tracks = tracks;
		
		this.timeLimit = timeLimit;
		this.seed = seed;
		
		this.startTime =  System.nanoTime();
		this.rand = new Random(System.nanoTime());
	}
	
	public ArrayList<ArrayList<Integer>> run() {
		ArrayList<ArrayList<Integer>> bestSolution = new ArrayList<>();
		double bestEval = 0;
		boolean first = true;
		int counter = 0;
		int evalCounter = 0;
		boolean hasFailure = false;
		while(counter < timeLimit ) {
			
			
			try {
				hasFailure = constructionPhase(seed);
				
			} catch (NonFeasibleException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(!hasFailure) {
				if(first) {
					bestSolution.clear();
					bestEval = evaluate(tracks);
					//bestSolution.addAll(tracks);
					for (Track t : tracks) {
						ArrayList<Integer> track = new ArrayList<>();
						for(Vehicle v : t.getVehicles()) {
							track.add(v.getId());
						}
						bestSolution.add(track);
					}
					first=false;
					System.out.println("BestSolution: "+bestEval +" ;F: "+this.bestF+ " ;G: "+this.bestG +"; Counter:" +evalCounter);
					int sumVeh=0;
					for(int i = 0; i < bestSolution.size();++i) {
						sumVeh += bestSolution.get(i).size();
					}
					System.out.println("SUMVEH:_"+sumVeh);
				}else {
					double currentEval = evaluate(tracks);
					if(bestEval < currentEval) {
						bestSolution.clear();
						bestEval = currentEval;
						for (Track t : tracks) {
							ArrayList<Integer> track = new ArrayList<>();
							for(Vehicle v : t.getVehicles()) {
								track.add(v.getId());
							}
							bestSolution.add(track);
						}
						this.bestF = this.F;
						this.bestG = this.G;
						System.out.println("BestSolution: "+bestEval +" ;F: "+this.bestF+ " ;G: "+this.bestG +"; Counter:" +evalCounter);
						int sumVeh=0;
						for(int i = 0; i < bestSolution.size();++i) {
							sumVeh += bestSolution.get(i).size();
						}
						System.out.println("SUMVEH:_"+sumVeh);
					}
				}
				
				evalCounter++;
			}
			counter++;
		}
		System.out.println("LAST BestSolution: "+bestEval +" ;F: "+this.bestF+ " ;G: "+this.bestG +"; Counter:" +evalCounter);
		int sumVeh=0;
		for(int i = 0; i < bestSolution.size();++i) {
			sumVeh += bestSolution.get(i).size();
		}
		System.out.println("SUMVEH:_"+sumVeh);
		return bestSolution;
	}
	
	

	public boolean constructionPhase(long seed) throws NonFeasibleException{
		for (Track t : tracks) {
			t.resetTrack();
		}
		Collections.sort(vehicles);
		int counterCars = 0;
		for (Vehicle v : vehicles) {
			ArrayList<Track> possibleTracks = getPossibleTracks(v);
			if(possibleTracks.size()!= 0) {
				counterCars++;
				int randIndex = rand.nextInt(possibleTracks.size());
				tracks.get(possibleTracks.get(randIndex).getId()-1).addVehicleGRASP(v);

			}else {
				return true;
			}
		}
		return false;
	}
	
	private ArrayList<Track> getPossibleTracks(Vehicle v) {
		ArrayList<Track> possibleTracks = new ArrayList<>();
		ArrayList<Track> blockingTracks = (ArrayList<Track>) tracks.stream().filter(t->t.getIsBlocking() == true && t.getActiveVehicleType() == -1 || t.getActiveVehicleType() == v.getType()).collect(Collectors.toList());
		ArrayList<Track> nonBlockingTracks = (ArrayList<Track>) tracks.stream().filter(t->t.getIsBlocking() == false && t.getActiveVehicleType() == -1 || t.getActiveVehicleType() == v.getType()).collect(Collectors.toList() );
		if(blockingTracks.size() != 0) {
			for(int i = 0; i < blockingTracks.size();++i) {
				int j = blockingTracks.get(i).getId()-1;
				//Only adequate vehicle on track and only one type of vehicle on track
				if(tracks.get(j).getAllowedVehicles().contains(v) && 
						(tracks.get(j).getActiveVehicleType() == -1 || tracks.get(j).getActiveVehicleType() == v.getType())) {
					//Only to certain length of track
					if(tracks.get(j).getCurrentLength() + v.getLength() <= tracks.get(j).getMaxLength()) {
						possibleTracks.add(tracks.get(j));
					}
				}
			}
		}
		
		if(blockingTracks.size() == 0 || possibleTracks.size() == 0){
			for(int i = 0; i < nonBlockingTracks.size();++i) {
				int j = nonBlockingTracks.get(i).getId()-1;
				//Only adequate vehicle on track and only one type of vehicle on track
				if(tracks.get(j).getAllowedVehicles().contains(v) && 
						(tracks.get(j).getActiveVehicleType() == -1 || tracks.get(j).getActiveVehicleType() == v.getType())) {
					//Only to certain length of track
					if(tracks.get(j).getCurrentLength() + v.getLength() <= tracks.get(j).getMaxLength()) {
						possibleTracks.add(tracks.get(j));
					}
				}
			}
		}
		
		return possibleTracks;
	}
	
	private double evaluate(ArrayList<Track> tracks) {
		F = 0;
		double f1 = 0;
		double f2 = 0;
		double f3 = 0;
		double p1 = 0;
		double p2 = 0;
		double p3 = 0;
		G = 0;
		double g1 = 0;
		double g2 = 0;
		double g3 = 0;
		double r1 = 0;
		double r2 = 0;
		double r3 = 0;
		int vehiclesSum = 0;
		
		
		ArrayList<Track> emptyTracks = (ArrayList<Track>) tracks.stream().filter(t -> t.getVehicles().size() == 0).collect(Collectors.toList());
		ArrayList<Track> fullTracks = (ArrayList<Track>) tracks.stream().filter(t -> t.getVehicles().size() != 0).collect(Collectors.toList());
		
		for(int i = 0; i < fullTracks.size(); ++i) {
			if( i > 0 ) {
				if(fullTracks.get(i).getActiveVehicleType() != fullTracks.get(i-1).getActiveVehicleType()) {
					f1++;
				}
				Vehicle v1 = fullTracks.get(i).getVehicles().get(0);
				Vehicle v2 = fullTracks.get(i-1).getVehicles().get(fullTracks.get(i-1).getVehicles().size() - 1);
				
				if(v1.getScheduleType() == v2.getScheduleType()) {
					g2++;
				}
			}
			f3+= fullTracks.get(i).getMaxLength() - fullTracks.get(i).getCurrentLength();
			p3+= fullTracks.get(i).getMaxLength();
			r1 += fullTracks.get(i).getVehicles().size();
			for(int j = 1; j < fullTracks.get(i).getVehicles().size(); ++j) {
				r3++;
				Vehicle v1 = fullTracks.get(i).getVehicles().get(j);
				Vehicle v2 = fullTracks.get(i).getVehicles().get(j-1);
				
				if(v1.getScheduleType() == v2.getScheduleType()) {
					g1++;
				}
				
				if((v1.getTime() - v2.getTime() >= 10) && (v1.getTime() - v2.getTime() <=20)) {
					g3+=15;
				}else {
					if(v1.getTime() - v2.getTime() > 20) {
						g3+=10;
					}else if (v1.getTime() - v2.getTime() < 10) {
						g3+= -4*(10 - (v1.getTime() - v2.getTime()));
					}
				}
			}
		}
		
		for(int i = 0; i < emptyTracks.size(); ++i) {
			p3+= emptyTracks.get(i).getMaxLength();
		}
		
		for(int i = 0; i < vehicles.size();++i) {
			vehiclesSum+= vehicles.get(i).getLength();
		}
		
		
		f2 = fullTracks.size();
		p1 = 1/(f2 - 1);
		p2 = 1/tracks.size();
		p3 = 1/(p3-vehiclesSum);
		
		F = p1*f1 + p2*f2 + p3*f3;
		
		r3 = 1/(15*r3);
		r2 = 1/(f2-1);
		r1 = 1/(r1 - f2);
		
		G = r1*g1 + r2*g2 + r3*g3;
		
		return G/F;
	}
}

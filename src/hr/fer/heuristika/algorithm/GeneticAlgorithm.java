package hr.fer.heuristika.algorithm;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import hr.fer.heuristika.domain.*;

public class GeneticAlgorithm {
	private List<Vehicle> vehicles;
	private List<Track> tracks;
	private int populationSize;
	private double mutationProb;
	@SuppressWarnings("unused")
	private double numIterations;
	private int elitism;
	private double totalFitness;
	
	private List<Solution> solutions;
	
	int counter;
	
	public GeneticAlgorithm(List<Vehicle> vehicles, List<Track> tracks, int populationSize
			, double mutationProb, int numIterations, int elitism) {
		this.vehicles = vehicles;
		this.tracks = tracks;
		this.populationSize = populationSize;
		this.mutationProb = mutationProb;
		this.numIterations = numIterations;
		this.elitism = elitism;
		this.solutions = new ArrayList<>();
	}
	
	public void run() throws FileNotFoundException {
		int i = -1;
		initialization();
		while(best().getFitness() > 0) {
			step();
			i++;
			
			if (i % 1000 == 0) {
				System.out.println(i + " fitness: " + best().getFitness());
			}
			
			
			removeDuplicates();
			
		}
		System.out.println("Best solution have fitness: " + best().getFitness());
		System.out.println(best());
	}
	
	private void initialization() {
		for (int i = 0; i < populationSize; i++) {
			Solution a = new Solution(vehicles, tracks);
			a.generateRandom();
			solutions.add(a);
		}
	}
	
	private void removeDuplicates() {
		for (int i = 0; i < solutions.size(); i++) {
			for (int j = i+1 ; j < solutions.size(); j++) {
				if (solutions.get(i).checkMatrix(solutions.get(j))) {
					solutions.set(i, crossover(selectParents()));
				}
			}
		}
	}
	
	private void step() {
		List<Solution> newSolutions = new ArrayList<>();
		calculateFitness();
		solutions.sort(null);
		
		newSolutions.addAll(bestN());
		
		while(newSolutions.size() != populationSize) {
			ParentsTuple parents = selectParents();
			Solution child = crossover(parents);
			mutate(child);
			
			newSolutions.add(child);
		}
		solutions = newSolutions;
	}
	private void calculateFitness() {
		totalFitness = 0;
		for (int i = 0; i < solutions.size(); i++) {
			solutions.get(i).calculateFitness();
			totalFitness += solutions.get(i).getFitness();
		}
	}
	private List<Solution> bestN() {
		return solutions.subList(0, elitism);
	}
	public Solution best() {
		return solutions.get(0);
	}
	//3-turnir
	private ParentsTuple selectParents() {
		/*
		Random rand = new Random(System.currentTimeMillis());
		Solution s1 = solutions.get(rand.nextInt(solutions.size()));
		Solution s2 = solutions.get(rand.nextInt(solutions.size()));
		Solution s3 = solutions.get(rand.nextInt(solutions.size()));
		
		Solution best1 = s1.getFitness() < s2.getFitness() ? s1 : s2;
		Solution best2;
		if(best1.equals(s1)) {
			best2 = s2.getFitness() < s3.getFitness() ? s2 : s3;
		} else {
			best2 = s1.getFitness() < s3.getFitness() ? s1 : s3;
		}
		
		return new ParentsTuple(best1, best2);
		*/
		Random rand = new Random(System.currentTimeMillis());
		double num1 = rand.nextDouble();
		double num2 = rand.nextDouble();
		Solution best1 = null;
		Solution best2 = null;
		
		
		double tmp = 0;
		for (Solution s : solutions) {
			tmp +=  (s.getFitness() / totalFitness);
			if (tmp >= num1 && best1 == null) {best1 = s; continue;}
			if (tmp >= num2 && best2 == null) {best2 = s; continue;}
			if (best1 != null && best2 != null) break;
		}
		best1 = best1 == null ? solutions.get(rand.nextInt(solutions.size())) : best1;
		best2 = best2 == null ? solutions.get(rand.nextInt(solutions.size())) : best2;
		return new ParentsTuple(best1, best2);
	}
	private Solution crossover(ParentsTuple parents) {
		int[] newSolutions = new int[parents.s1.getVehiclesNumber()];
		Random rand = new Random(System.currentTimeMillis());
		int breakPoint = rand.nextInt(parents.s1.getVehiclesNumber());
		for (int i =0; i < parents.s1.getVehiclesNumber(); i++) {
			newSolutions[i] = i < breakPoint ? parents.s1.matrixSolution[i] : parents.s2.matrixSolution[i]; 
		}
		
		Solution result = new Solution(parents.s1.getVehicles(), parents.s1.getTracks());
		result.setMatrixSolution(newSolutions);
		
		return result;
	}
	private void mutate(Solution solution) {
		Random rand = new Random(System.currentTimeMillis());
		for (int i = 0; i < solution.getVehiclesNumber(); i++) {
			if (mutationProb >= rand.nextDouble()) {
				solution.matrixSolution[i] = vehicles.get(i).getRandomAlowedTrack(rand);
			}
		}
	}
}





















package hr.fer.heuristika;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import hr.fer.heuristika.utils.InputReaderCustom;
import hr.fer.heuristika.utils.OutputWriterCustom;


public class putnik {

	public static void main(String[] args) throws IOException {
		InputReaderCustom reader = new InputReaderCustom(System.in);

		@SuppressWarnings("unused")
		int brojgradova = reader.readInt();
		int brojCesta = reader.readInt();

		Map<Integer, Veza> gradovi = new HashMap<>(500);

		int prviGrad, drugiGrad, veza;
		for (int i = 0; i < brojCesta; i++) {

			prviGrad = reader.readInt();
			drugiGrad = reader.readInt();
			veza = reader.readInt();

			Veza vezePrvogGrada = gradovi.get(prviGrad);
			if (vezePrvogGrada == null) {
				vezePrvogGrada = new Veza();
				gradovi.put(prviGrad, vezePrvogGrada);
			}
			Veza vezeDrugogGrada = gradovi.get(drugiGrad);
			if (vezeDrugogGrada == null) {
				vezeDrugogGrada = new Veza();
				gradovi.put(drugiGrad, vezeDrugogGrada);
			}

			Integer prvi = vezePrvogGrada.veze.get(drugiGrad);
			if (prvi == null) {
				vezePrvogGrada.veze.put(drugiGrad, veza);
				vezeDrugogGrada.veze.put(prviGrad, veza);
			} else {
				if (prvi > veza) {
					vezePrvogGrada.veze.put(drugiGrad, veza);
					vezeDrugogGrada.veze.put(prviGrad, veza);
				}
			}
		}

		pokreniDijsktru(gradovi);

	}

	public static void pokreniDijsktru(Map<Integer, Veza> gradovi) {
		Long sumaMin = Long.MAX_VALUE;
		int najmanjiGrad = 0;
		
		for (Entry<Integer, Veza> a : gradovi.entrySet()) { //svaki cvor kao pocetni
			int trenutniGrad = a.getKey();
			Veza trenutneVeze = a.getValue();
			
			Long suma = (long) 0;
			Long trenutnaUdaljenost = (long) 0;

			Map<Integer, Long> skupNepredenih = new HashMap<>();
			for (Integer grad : gradovi.keySet()) {
				skupNepredenih.put(grad, Long.MAX_VALUE);
			}
			skupNepredenih.remove(trenutniGrad);

			while (!skupNepredenih.isEmpty()) {
				for (Entry<Integer, Integer> vezeGledajucegCvora : trenutneVeze.veze.entrySet()) {
					Long vezaPrema = skupNepredenih.get(vezeGledajucegCvora.getKey());
					if (vezaPrema == null) continue;
					if (vezaPrema > vezeGledajucegCvora.getValue() + trenutnaUdaljenost) {
						vezaPrema = (long) (vezeGledajucegCvora.getValue() + trenutnaUdaljenost);
						skupNepredenih.replace(vezeGledajucegCvora.getKey(), vezaPrema);
					}
				}
				
				Long minValue = Long.MAX_VALUE;
				int minGrad = 0;
				for (Entry<Integer, Long> cvor : skupNepredenih.entrySet()) {
					if (cvor.getValue() < minValue) {
						minValue = cvor.getValue();
						minGrad = cvor.getKey();
					}
				}
				
				suma += minValue;
				if (suma > sumaMin) break;
				
				trenutnaUdaljenost = minValue;
				skupNepredenih.remove(minGrad);
				trenutneVeze = gradovi.get(minGrad);
			}

			if (suma <= sumaMin) {
				if (suma == sumaMin) {
					if (najmanjiGrad > trenutniGrad) {
						najmanjiGrad = trenutniGrad;
						sumaMin = suma;
					}
				} else {
					najmanjiGrad = trenutniGrad;
					sumaMin = suma;
				}
			}
		}
		OutputWriterCustom writer = new OutputWriterCustom(System.out);
		
		writer.printLine(najmanjiGrad + " " + sumaMin*2);
		writer.flush();
		writer.close();
	}

	public static class Veza {
		Map<Integer, Integer> veze = new HashMap<>();
	}




}
